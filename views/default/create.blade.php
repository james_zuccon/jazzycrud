@include('jazzycrud::' . $config['theme'] . '.functions')

@if ($config['themeOptions']['loadBootstrap'])
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
@endif

<form method="POST" enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

@if (isset($config['renderHooks']['create']['before']))
  {!! $config['renderHooks']['create']['before'] !!}
@endif

{{-- Start Nav Tabs --}}
@if (count($config['tabs']))
<ul class="nav nav-tabs" role="tablist">
  <?php $tabNumber = 0; ?>
  @foreach ($config['tabs'] as $name=>$columns)
  <li role="presentation" class="{{ ($tabNumber == 0) ? 'active' : '' }}"><a href="#{{ str_slug($config['jcid'] . '-' . $name) }}" aria-controls="{{ str_slug($config['jcid'] . '-' . $name) }}" role="tab" data-toggle="tab">{{ $name }}</a></li>
  <?php $tabNumber++; ?>
  @endforeach
</ul>
@endif
{{-- End Nav Tabs --}}

@if (count($config['tabs']))
<div class="tab-content">

    <?php $tabNumber = 0; ?>
    @foreach ($config['tabs'] as $name=>$columns)
    <div role="tabpanel" class="tab-pane {{ ($tabNumber == 0) ? 'active' : '' }}" id="{{ str_slug($config['jcid'] . '-' . $name) }}">
    
        {!! renderWidgets($config, $columns, $entity) !!}
        
    </div>
    <?php $tabNumber++; ?>
    @endforeach
</div>

@else

{!! renderWidgets($config, array_flip($creates), $entity) !!}

@endif

@if (isset($config['renderHooks']['create']['after']))
  {!! $config['renderHooks']['create']['after'] !!}
@endif

<div class="text-right">
    @foreach ($entity->actions as $name=>$action)
        {!! renderAction($config['jcid'], $name, $action) !!}
    @endforeach
</div>

</form>