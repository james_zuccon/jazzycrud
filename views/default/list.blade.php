@include('jazzycrud::' . $config['theme'] . '.functions')

@if ($config['themeOptions']['loadJQuery'])
<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
@endif

@if ($config['themeOptions']['loadBootstrap'])
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
@endif

@if ($config['themeOptions']['loadDataTables'])
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.css">
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
@endif

@if (\Session::has($config['table-id']))
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  {{ \Session::get($config['table-id']) }}
</div>
@endif

<style>
    #table td { vertical-align:middle; }
</style>

@if (\Input::has('_msg'))
<div class="alert alert-info fade in">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    {{ \Input::get('_msg') }}
</div>
@endif

@if (isset($config['renderHooks']['list']['before']))
  {!! $config['renderHooks']['list']['before'] !!}
@endif

<table id="table" class="table table-striped table-bordered table-list" width="100%" cellspacing="0">
    <thead>
        <tr>
            @foreach ($lists as $name=>$select)
            <th>{{ (isset($config['titles'][$name])) ? $config['titles'][$name] : cleanTitle($name) }}</th>
            @endforeach
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($results as $result)
        <tr>
            @foreach ($lists as $display=>$select)
            <td>
            @if ($result->{$display} && isset($config['rules'][$display]['type']) && $config['rules'][$display]['type'] == 'image_upload')
                <img src="{{ asset($config['uploadsUrl'] . $result->{$display}) }}" class="image" />
            @else
                {{ $result->{$display} }}
            @endif
            </td>
            @endforeach
            <td class="text-center" style="white-space:nowrap; width:1%;">
                @if (isset($result->actions))
                @foreach ($result->actions as $name=>$action)
                    @if (isset($action['type']) && $action['type'] == 'custom')
                    <a class="btn btn-default" href="{{ parseUrl($name, $result) }}" title="{{ $action['title'] }}">
                        <i class="glyphicon glyphicon-{{ $action['classes'] }}"></i>
                    </a>
                    @else
                    <a class="btn btn-default jazzylink" href="{{ URL::to(URL::current() . '?_jcid=' . $config['jcid'] . '&_action=' . parseUrl($name, $result) . '&' . $action['params']) }}"
                        {!! (isset($action['prompt'])) ? 'data-prompt="'.$action['prompt'].'"' : '' !!} >
                        <i class="glyphicon glyphicon-{{ $action['classes'] }}"  title="{{ $action['title'] }}"></i>
                    </a>
                    @endif
                @endforeach
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@if (isset($config['renderHooks']['list']['after']))
  {!! $config['renderHooks']['list']['after'] !!}
@endif

@if (array_search('CREATE', $config['allowed']))
<div class="text-right" style="margin-top:0.5em">
    <a class="btn btn-primary jazzylink" href="{{ URL::to(URL::current() . '?_jcid=' . $config['jcid'] . '&_action=create') }}">Create a new entry</a>
</div>
@endif

<script type="text/javascript" charset="utf-8">
    $('#table').dataTable({ "iDisplayLength": 50 });
</script>