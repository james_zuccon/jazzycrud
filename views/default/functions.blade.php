<?php

function cleanTitle($columnName)
{
    return ucwords(str_replace(['-', '_'], ' ', $columnName));
}

function renderAction($jcid, $name, $action) {

    // If there are params, get them
    $params = '';
    if (isset($action['params'])) {
        $params = $action['params'];
    }
    
    $prompt = '';
    if (isset($action['prompt'])) {
        $prompt = 'data-prompt="'.$action['prompt'].'"';
    }

    // If it is a button
    if (isset($action['type']) && $action['type'] === 'button') {
        return '<button class="btn btn-default jazzylink" formaction="' .  URL::to(URL::current()) . '?_jcid=' . $jcid . '&_action=' . $name . '&' . $params . '" ' . $prompt . ' >' . $action['title'] . '</button>';
    }
    
    // Otherwise it is a link
    else {
        return '<a class="btn btn-default jazzylink" href="' . URL::to(URL::current()) . '?_jcid=' . $jcid . '&_action=' . $name . '&' . $params . '" ' . $prompt . ' >' . $action['title'] . '</a>';
    }
}

function renderWidgets($config, $columns, $entity)
{
    $widgets = '<table class="table table-striped table-bordered" style="table-layout:fixed;">
                  <tbody>';
    
    foreach ($columns as $name) {
    
        $title = (isset($config['titles'][$name])) ? $config['titles'][$name] : cleanTitle($name);
    
        $widgets .= '<tr>
                       <th style="width:20%;">' . $title . '</th>
                       <td>' . renderWidget($config, $name, $entity) . '</td>
                     </tr>';
    
    }
    
    $widgets .= ' </tbody>
                </table>';

    return $widgets;
}

function renderWidget($config, $name, $entity = NULL) {

    // Setup commonly used variables
    $rules = $config['rules'][$name];

    // Set up return code
    $widget = '';
    
    // If there's an entity, get its value
    if (isset($entity->$name)) {
        $value = $entity->$name;
    }
    
    // Otherwise, use the default (or set default manually)
    else {
        $value = (isset($rules['default'])) ? $rules['default'] : '';
    }
    
    // Setup placeholders
    $placeholder = '';
    if (isset($config['placeholders'][$name])) {
        $placeholder = ' placeholder="' . $config['placeholders'][$name] . '" ';
    }
    
    // Setup required
    if (isset($rules['required']) && $rules['required']) {
        $required = ' required ';
    } else {
        $required = '';
    }
    
    // If there's a 'before' hook, render it
    if (isset($config['columnHooks'][$name]['before'])) {
        $widget .= $config['columnHooks'][$name]['before'];
    }
    
    if (isset($rules['type'])) {
        switch ($rules['type']) {
        
            case 'checkbox':
                $checked = '';
                ($value) ? $checked = 'checked' : '';
                $widget .= '<input type="checkbox" name="' . $name . '" value="1" ' . $checked . ' ' . $required . ' />';
                break;
        
            case 'date':
                $widget .= '<input type="date" name="' . $name . '" value="' . $value . '" ' . $required . ' />';
                break;
        
            case 'integer':
                $widget .= '<input class="form-control" type="number" name="' . $name . '" value="' . $value . '" ' . $required . ' />';
                break;
                
            case 'double':
                $widget .= '<input class="form-control" type="number" step="any" name="' . $name . '" value="' . $value . '" ' . $required . ' />';
                break;
            
            case 'string':
                $widget .= '<input class="form-control" type="text" name="' . $name . '" value="' . $value . '" ' . $placeholder . ' ' . $required . ' />';
                break;
            
            case 'textarea':
                $widget .= '<textarea class="form-control" name="' . $name . '" ' . $required . $placeholder . ' >' . $value . '</textarea>';
                break;
            
            case 'select':
                $widget .= '<select class="form-control" name="' . $name . '" ' . $required . ' >';
                foreach ($rules['options'] as $key=>$title) {
                    $selected = '';
                    ($key == $value) ? $selected = 'selected' : '';
                    $widget .= '<option value="' . $key . '" ' . $selected . ' >' . $title . '</option>'; 
                }
                $widget .= '</select>';
                break;
                
            case 'image':
                if (strlen($value)) {
                    $widget .= '<img src="' . $value . '" title="' . $value . '" style="max-width:100%; margin-bottom:1em;" />';
                }
                $widget .= '<input class="form-control" type="text" name="' . $name . '" value="' . $value . '" ' . $placeholder . ' />';
                //$image .= '<input type="file" name="' . $name . '">';
                break;
                
            case 'image_upload':
                if (strlen($value)) {
                    $widget .= '<img src="' . asset($config['uploadsUrl'] . $value) . '" title="' . $value . '" style="max-width:100%; margin-bottom:1em;" />';
                }
                $widget .= '<input type="file" name="' . $name . '">';
                break;
                
            case 'file_upload':
                if (strlen($value)) {
                    $widget .= '<a href="' . asset($config['uploadsUrl'] . $value) . '" title="' . $value . '" />' . $value . '</a>';
                }
                $widget .= '<input type="file" name="' . $name . '">';
                break;
                
            case 'code':
                $widget .= '<textarea name="' . $name . '" id="' . $name . '" ' . ' >' . $value . '</textarea>' .
                '<script>
                  var editor = CodeMirror.fromTextArea(document.getElementById("' . $name . '"), {
                    lineNumbers: true,';
                    if (isset($rules['extra']['mode'])) {
                        $widget .= 'mode: "' . $rules['extra']['mode'] . '",';
                    }
                $widget .=  '});
                </script>';
                break;
        }
    }
    
    // If there's a 'after' hook, render it
    if (isset($config['columnHooks'][$name]['after'])) {
        $widget .= $config['columnHooks'][$name]['after'];
    }
    
    return $widget;
}

function parseUrl($url, $row)
{
    $url = preg_replace_callback("/{{\s*([0-9a-zA-Z-_]*)\s*}}/", function ($match) use ($row) {

            // If the array key exists return it
            if (property_exists($row, (string)$match[1]))
                return (string)$row->{(string)$match[1]};
                
            // Otherwise just print the name of the variable 
            else
                return (string)$match[1];
                
    }, $url);
    
    return $url;
}

?>