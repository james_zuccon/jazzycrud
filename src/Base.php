<?php namespace Jimtendo\JazzyCRUD;

/**
* A terribly useful class
*
* See: http://knpuniversity.com/screencast/question-answer-day
*/
class Base
{
    /**
    * Configuration settings
    *
    * Valid array options are:
    * ['theme'] = 'default' (Theme to use)
    * ['table'] = 'table_name' (Table name)
    * ['primaryKey'] = 'id' (Primary key for table)
    * ['rules'] = [ 'column_name'=>['type'='input, ...] (The rules that should apply to each column)
    * ['lists'] = [ ['id'=>'id', 'name'=>'name'] ] (Fields to show on list as 'NAME'=>'MYSQL Statement')
    * ['creates'] = [ ['id'=>'id', 'name'=>'name'] ] (Fields to show on create as 'NAME'=>'MYSQL Statement')
    * ['edits'] = [ ['id'=>'id', 'name'=>'name'] ] (Fields to show on create as 'NAME'=>'MYSQL Statement')
    * ['where'] = 'some_field = some_value' (Where statement)
    * ['joins'] = [ ['table'=>'foreignTable', 'localKey'=>'local_id', 'foreignKey'=>'foreign_id'] ] (Array of joins to perform)
    * ['relations'] = [ 'column'=>['foreignTable'=>'table_name', 'foreignKey'=>'foreign_id', 'foreignTitle'=>'name', (OPTIONAL) 'where'=>'club_id = 12', (OPTIONAL) 'order'=>false] ]
    * ['titles'] = [ 'column'=>'Title', ... ] (Titles for each column)
    * ['tabs'] = [ 'Tab Name'=>['column_one', 'column_two', 'column_three'] ] (Tabs to place the columns under)
    * ['placeholders'] = ['column'=>'Placeholder', ... ] (Placeholders for each column)
    * ['transformations'] = ['action'=>['column'=>function($value) { return '--' . $value . '--'; }]]
    * ['callbacks'] = ['beforeStore'=>function(&$data)] (Callbacks for modifying data before/after action)
    * ['renderHooks'] = [ 'list'=>['before'=>'Print this text before listing the rows',
    *                              'after'=>'Print this text after listing the rows'],
    *                     'edit'=>['before'=>'Print this text before rendering the edit widgets'] ]
    * ['columnHooks'] = [ 'column'=>['before'=>'Print this text',
    *                                'after'=>'Print this text'] ]
    * ['valueHooks'] = [ 'list'=>['column'=>function() { return 'new_value'; }] ]
    * ['softDeletes'] = true/false
    */
    protected $config;
    
    /**
    * Associative Array of columns and formatting functions (COLUMN NAME => FUNCTION(COLUMN_VALUE) { RETURN FORMATTED(COLUMN_VALUE) }
    */
    protected $customizations = array();
    
    /**
    * Associative Array of columns we will list (COLUMN NAME => SELECT STATEMENT)
    */
    protected $hooks = array();
    
    /**
    * List of actions
    */
    protected $actions;
    
    /**
    * Callback hooks
    */
    protected $callbacks;
    
    /**
    * Action to perform
    */
    protected $action;
    
    /**
    * Entity to perform action on
    */
    protected $id;
    
    function __construct($jcid, $config = array())
    {
        // Get configuration
        $this->config = config('jazzycrud');
    
        // Set HTML table settings
        $this->config['table-id'] = 'jc-basic';
    
        // Merge defaults with user specified settings
        $this->config = array_merge($this->config, $config);
        
        // Set the name of this JazzyCRUD instance
        $this->config['jcid'] = $jcid;
        
        // Setup options
        $this->config['table'] = 'table';
        $this->config['rules'] = array();
        $this->config['lists'] = array();
        $this->config['creates'] = array();
        $this->config['edits'] = array();
        $this->config['joins'] = array();
        $this->config['relations'] = array();
        $this->config['orderBy'] = array();
        $this->config['defaults'] = array();
        $this->config['values'] = array();
        $this->config['tabs'] = array();
        
        // Setup actions array
        $this->actions['list'] = array();
        $this->actions['create'] = array();
        $this->actions['show'] = array();
        $this->actions['edit'] = array();
    }
    
    public function onRender($function)
    {
        // If it's an AJAX request
        if (\Input::get('_jcid') == $this->config['jcid']) {
            $function();
            $this->render();
        }
    }
    
    public function container()
    {
        return '<div id="' . $this->config['jcid'] . '" data-url="' . \URL::current() . '?_jcid=' . $this->config['jcid'] . '"></div>' .
               '<script>' .
               '     $("#' . $this->config['jcid'] . '").jazzyBox();' .
               '</script>';
    }
    
    public function action($action, $id = NULL)
    {
        $this->action = $action;
        $this->id = $id;
        
        return $this;
    }
    
    /**
    * Set the name of the table
    */
    public function from($table)
    {
        $this->config['table'] = $table;
    
        return $this;
    }
    
    /**
    * Join statements for table
    */
    public function join($array)
    {
        $this->config['joins'][] = $array;
    
        return $this;
    }
    
    /**
    * Where statement for the table
    */
    public function where($condition)
    {
        $this->config['where'] = $condition;
    
        return $this;
    }
    
    public function lists($columns)
    {
        $this->config['lists'] = $columns;
        
        return $this;
    }
    
    public function creates($columns)
    {
        $this->config['creates'] = $columns;
        
        return $this;
    }
    
    public function shows($columns)
    {
        $this->config['shows'] = $columns;
        
        return $this;
    }
    
    public function edits($columns)
    {
        $this->config['creates'] = $columns;
        
        return $this;
    }
    
    public function title($column, $title)
    {
        $this->config['titles'][$column] = $title;
        
        return $this;
    }
    
    public function titles($titles)
    {
        $this->config['titles'] = $titles;
        
        return $this;
    }
    
    public function placeholder($column, $placeholder)
    {
        $this->config['placeholders'][$column] = $placeholder;
        
        return $this;
    }
    
    public function placeholders($placeholders)
    {
        $this->config['placeholders'] = $placeholders;
        
        return $this;
    }
    
    public function rule($column, $options)
    {
        $this->config['rules'][$column] = $options;
        
        return $this;
    }
    
    public function rules($rules)
    {
        $this->config['rules'] = $rules;
        
        return $this;
    }
    
    public function allow($actions)
    {
        if (!is_array($actions)) {
            $actions = array($actions);
        }
        
        foreach ($actions as $action) {
            if (array_search($action, $this->config['allowed']) == false) {
                $this->config['allowed'][] = $action;
            }
        }
        
        return $this;
    }
    
    public function disallow($actions)
    {
        if (!is_array($actions)) {
            $actions = array($actions);
        }
        
        foreach ($actions as $action) {
            if(($key = array_search($action, $this->config['allowed'])) !== false) {
                unset($this->config['allowed'][$key]);
            }
        }
        
        return $this;
    }
    
    public function allowed($actions)
    {
        if (!is_array($actions)) {
            $actions = array($actions);
        }
        
        $this->config['allowed'] = $actions;
        
        return $this;
    }
    
    public function relation($column, $options)
    {
        $this->config['relations'][$column] = $options;
        
        return $this;
    }
    
    public function relations($array)
    {
        $this->config['relations'] = $relations;
        
        return $this;
    }
    
    public function customize($column, $function)
    {
        $this->customizations[$column] = $function;
        
        return $this;
    }
    
    public function orderBy($array)
    {
        $this->config['orderBy'] = $array;
        
        return $this;
    }
    
    public function setDefault($column, $value)
    {
        $this->config['rules'][$column]['default'] = $value;
        
        return $this;
    }
    
    public function defaults($defaults)
    {
        $this->config['defaults'] = $defaults;
        
        return $this;
    }
    
    public function addAction($on, $action, $options)
    {
        $this->actions[$on][$action] = $options;
        
        return $this;
    }
    
    public function setValue($column, $value)
    {
        $this->config['values'][$column] = $value;
        
        return $this;
    }
    
    public function setValues($array)
    {
        $this->config['values'] = $array;
        
        return $this;
    }
    
    public function tab($name, $columns)
    {
        $this->config['tabs'][$name] = $columns;
        
        return $this;
    }
    
    public function unescape($columns)
    {
        if (!is_array($columns)) {
            $columns = array($columns);
        }
    
        foreach ($columns as $column) {
            $this->config['rules'][$column]['escape'] = false;
        }
        
        return $this;
    }
    
    public function transform($actions, $columns, $function)
    {
        if (!is_array($columns)) {
            $columns = array($columns);
        }
    
        if (!is_array($actions)) {
            $actions = array($actions);
        }
        
        foreach ($actions as $action) {
            foreach ($columns as $column) {
                $this->config['transformations'][$action][$column] = $function;
            }
        }
        
        return $this;
    }
    
    public function callback($actions, $function)
    {
        if (!is_array($actions)) {
            $actions = array($actions);
        }
        
        foreach ($actions as $action) {
            $this->config['callbacks'][$action] = $function;
        }
        
        return $this;
    }
    
    public function renderHook($actions, $hook, $html)
    {
        if (!is_array($actions)) {
            $actions = array($actions);
        }
        
        foreach ($actions as $action) {
            $this->config['renderHooks'][$action][$hook] = $html;
        }
        
        return $this;
    }
    
    public function columnHook($column, $hook, $html)
    {
        $this->config['columnHooks'][$column][$hook] = $html;
        
        return $this;
    }
    
    public function valueHook($actions, $column, $hook)
    {
        if (!is_array($actions)) {
            $actions = array($actions);
        }
        
        foreach ($actions as $action) {
            $this->config['valueHooks'][$action][$column] = $hook;
        }
        
        return $this;
    }
    
    public function softDeletes($softDeletes = true)
    {
        $this->config['softDeletes'] = $softDeletes;
        
        return $this;
    }
    
    /**
    * Render the CRUD code
    *
    * Returns the final CRUD code for output to the screen
    *
    * @param  string  $action
    * @return string
    */
    public function render()
    {
        // Describe the fields (automatically get rules for validation, etc)
        $this->describe($this->config['table']);
    
        // If there is an action
        $action = $this->action;
        $id = $this->id;
    
        // If there is an action to perform, get it from $_GET 
        if (\Input::get('_action')) {
            $action = \Input::get('_action');
        }

        // If there is still no action, then LIST records
        if (!$action || $action == 'list') {
            return $this->performList();
        } else if ($action == 'create') { // CREATE
            return $this->performCreate();
        } else if ($action == 'store') { // STORE
            return $this->performStore();
        } else if ($action == 'show') { // SHOW
            return $this->performShow($id);
        } else if ($action == 'edit') { // EDIT
            return $this->performEdit($id);
        } else if ($action == 'update') { // UPDATE
            return $this->performUpdate();
        } else if ($action == 'destroy') { // DELETE
            return $this->performDestroy($id);
        } else {
            throw new \Exception('Invalid action given.');
        }
    }
    
    public function performList()
    {   
        // Ensure this is allowed
        if (!$this->isAllowed('LIST')) {
            abort(403, 'Listing is not allowed.');
        }
    
        // Setup query
        $query = $this->baseQuery($this->config['lists']);
        
        // Order the query
        foreach ($this->config['orderBy'] as $column=>$order) {
            $query->orderBy($column, $order);
        }
        
        // Actually get the results
        $results = $query->get();
        
        // Get relations
        foreach ($this->config['relations'] as $column=>$relation) {
            foreach ($results as $result) {
                if (isset($result->{$column})) {
                    $foreignObject = \DB::table($relation['foreignTable'])
                                            ->where($relation['foreignKey'], $result->{$column})
                                            ->first();
                    
                    if (is_callable($relation['foreignTitle'])) {
                        $result->{$column} = $relation['foreignTitle']($foreignObject);
                    } else {
                        $result->{$column} = $this->getForeignTitle($relation['foreignTitle'], $foreignObject);
                    }
                }
            }
        }
        
        // Perform transformations
        foreach ($results as &$entity) {
            if (isset($this->config['transformations']['list'])) {
                foreach ($this->config['transformations']['list'] as $column=>$function) {
                    $entity->{$column} = $function($entity->{$column});
                }
            }
        }
        
        // Setup default actions
        $primaryKey = $this->config['primaryKey'];
        
        if ($this->isAllowed('READ')) {
            $this->actions['list']['show'] = function($row) use ($primaryKey) {
                return [ 'title'=>'', 'classes'=>'search', 'params'=>'id=' . $row->$primaryKey ];
            };
        }
        
        if ($this->isAllowed('UPDATE')) {
            $this->actions['list']['edit'] = function($row) use ($primaryKey) {
                return [ 'title'=>'', 'classes'=>'edit', 'params'=>'id=' . $row->$primaryKey ];
            };
        }
        
        if ($this->isAllowed('DELETE')) {
            $this->actions['list']['destroy&after=list'] = function($row) use ($primaryKey) {
                return [ 'title'=>'', 'classes'=>'remove', 'params'=>'id=' . $row->$primaryKey, 'prompt'=>'Are you sure you want to delete this entry?' ];
            };
        }
        
        // Add actions for this row
        foreach ($results as $row) {
            foreach ($this->actions['list'] as $name=>$action) {
                    $row->actions[$name] = $action($row);
            }
        }
        
        // Render the results to a view
        die(view('jazzycrud::' . $this->config['theme'] . '.list', ['config'=>$this->config, 'lists'=>$this->config['lists'], 'results'=>$results])->render());
    }
    
    private function performCreate()
    {
        // Ensure this is allowed
        if (!$this->isAllowed('CREATE')) {
            abort(403, 'Creating new entries is not allowed.');
        }
    
        $entity;
        
        // Get relations
        $this->setupRelations();
    
        // Setup default actions
        $this->actions['create']['store&after=create'] = function() {
            return [ 'type'=>'button', 'title'=>'Save &amp; New', 'classes'=>'' ];
        };
        
        if ($this->isAllowed('LIST')) {
            $this->actions['create']['store&after=list'] = function() {
                return [ 'type'=>'button', 'title'=>'Save &amp; Return', 'classes'=>'' ];
            };
        }
        
        if ($this->isAllowed('LIST')) {
            $this->actions['create']['list'] = function() {
                return [ 'type'=>'a', 'title'=>'Return', 'classes'=>'', 'params'=>'' ];
            };
        }
        
        foreach ($this->actions['create'] as $name=>$action) {
            $entity->actions[$name] = $action();
        }
        
        // Render the view
        die(view('jazzycrud::' . $this->config['theme'] . '.create', ['config'=>$this->config, 'creates'=>$this->config['creates'], 'entity'=>$entity])->render());
    }
    
    private function performStore()
    {
        // Ensure this is allowed
        if (!$this->isAllowed('CREATE')) {
            abort(403, 'Creating new entries is not allowed.');
        }
        
        // Process the form input and get data to insert
        $data = $this->processFormInput($this->config['creates']); 

        // Give user opportunity to modify data before storing it
        if (isset($this->config['callbacks']['beforeStore']) && is_callable($this->config['callbacks']['beforeStore'])) {
            $this->config['callbacks']['beforeStore']($data);
        }
        
        // Perform the insert
        $entityId = \DB::table($this->config['table'])->insertGetId($data);
        
        // Call after data is stored
        if (isset($this->config['callbacks']['afterStore']) && is_callable($this->config['callbacks']['afterStore'])) {
            $this->config['callbacks']['afterStore']($entityId);
        }
        
        $this->redirect('Entry created successfully');
    }
    
    public function performShow($id = NULL)
    {
        // Ensure this is allowed
        if (!$this->isAllowed('READ')) {
            abort(403, 'Reading entries is not allowed.');
        }
    
        // Get id of entity to show if not specified
        if (!$id) $id = \Input::get('id');
        
        // Perform the query
        $query = $this->baseQuery();
        
        // Actually get the results
        $entity = $query->where($this->config['table'] . '.' . $this->config['primaryKey'], $id)->first();
        
        // Get relations
        foreach ($this->config['relations'] as $column=>$relation) {
            $foreignObject = \DB::table($relation['foreignTable'])
                                    ->where($relation['foreignKey'], $entity->{$column})
                                    ->first();
            
            if (is_callable($relation['foreignTitle'])) {
                $entity->{$column} = $relation['foreignTitle']($foreignObject);
            } else {
                $entity->{$column} = $this->getForeignTitle($relation['foreignTitle'], $foreignObject);
            }
        }
        
        // Perform customizations
        foreach ($this->customizations as $column=>$function) {
            $entity->{$column} = $function($entity->{$column});
        }
        
        // Perform transformations
        if (isset($this->config['transformations']['show'])) {
            foreach ($this->config['transformations']['show'] as $column=>$function) {
                $entity->{$column} = $function($entity->{$column});
            }
        }

        // Give user opportunity to modify data before showing it
        if (isset($this->config['callbacks']['afterShow']) && is_callable($this->config['callbacks']['afterShow'])) {
            $this->config['callbacks']['afterShow']($entity);
        }
        
        // Setup default actions
        $primaryKey = $this->config['primaryKey'];
        
        if ($this->isAllowed('DELETE')) {
            $this->actions['show']['destroy&after=list'] = function($row) use ($primaryKey) {
                return [ 'title'=>'Delete', 'classes'=>'remove', 'params'=>'id=' . $row->$primaryKey, 'prompt'=>'Are you sure you want to delete this entry?' ];
            };
        }
        
        if ($this->isAllowed('UPDATE')) {
            $this->actions['show']['edit'] = function($row) use ($primaryKey) {
                return [ 'title'=>'Edit', 'classes'=>'edit', 'params'=>'id=' . $row->$primaryKey ];
            };
        }
        
        if ($this->isAllowed('LIST')) {
            $this->actions['show']['list'] = function($row) use ($primaryKey) {
                return [ 'title'=>'Return', 'classes'=>'search', 'params'=>'id=' . $row->$primaryKey ];
            };
        }
        
        foreach ($this->actions['show'] as $name=>$action) {
            $entity->actions[$name] = $action($entity);
        }
        
        // Render the view
        die(view('jazzycrud::' . $this->config['theme'] . '.show', ['config'=>$this->config, 'shows'=>$this->config['shows'], 'entity'=>$entity]));
    }
    
    private function performEdit($id = NULL)
    {
        // Ensure this is allowed
        if (!$this->isAllowed('UPDATE')) {
            abort(403, 'Updating entries is not allowed.');
        }
    
        // Get id of entity to edit if not specified
        if (!$id) $id = \Input::get('id');
        
        //echo 'id: ' . $id; exit();
        
        // Perform the base query
        $query = $this->baseQuery();
        
        // Get the entity
        $entity = $query->where($this->config['table'] . '.' . $this->config['primaryKey'], $id)->first();
        
        // Call after data is stored
        if (isset($this->config['valueHooks']['edit'])) {
            foreach ($this->config['valueHooks']['edit'] as $column=>$hook) {
                $entity->{$column} = $hook($entity->{$column});
            }
        }
        
        // Get relations
        $this->setupRelations();
        
        // Setup default actions
        $primaryKey = $this->config['primaryKey'];
        
        // If updating is enabled, show buttons
        if ($this->isAllowed(['UPDATE', 'CREATE'])) {
            $this->actions['edit']['update&after=create'] = function($row) use ($primaryKey) {
                return [ 'type'=>'button', 'title'=>'Save &amp; New', 'classes'=>'', 'params'=>'id=' . $row->$primaryKey ];
            };
        }

        if ($this->isAllowed('UPDATE')) {
            $this->actions['edit']['update&after=edit'] = function($row) use ($primaryKey) {
                return [ 'type'=>'button', 'title'=>'Save &amp; Edit', 'classes'=>'', 'params'=>'id=' . $row->$primaryKey ];
            };
        }
            
        if ($this->isAllowed(['UPDATE', 'LIST'])) {
            $this->actions['edit']['update&after=list'] = function($row) use ($primaryKey) {
                return [ 'type'=>'button', 'title'=>'Save &amp; Return', 'classes'=>'', 'params'=>'id=' . $row->$primaryKey ];
            };
        }
        
        if ($this->isAllowed(['UPDATE', 'READ'])) {
            $this->actions['edit']['update&after=show'] = function($row) use ($primaryKey) {
                return [ 'type'=>'button', 'title'=>'Save', 'classes'=>'', 'params'=>'id=' . $row->$primaryKey ];
            };
        }
        
        // If listing is enabled, show buttons
        if (in_array('LIST', $this->config['allowed'])) {
            $this->actions['edit']['list'] = function($row) use ($primaryKey) {
                return [ 'type'=>'a', 'title'=>'Return', 'classes'=>'', 'params'=>'' ];
            };
        }
        
        foreach ($this->actions['edit'] as $name=>$action) {
            $entity->actions[$name] = $action($entity);
        }
        
        // Render the view
        die(view('jazzycrud::' . $this->config['theme'] . '.edit', ['config'=>$this->config, 'edits'=>$this->config['edits'], 'entity'=>$entity])->render());
    }
    
    private function performUpdate($id = NULL)
    {
        // Ensure this is allowed
        if (!$this->isAllowed('UPDATE')) {
            abort(403, 'Updating entries is not allowed.');
        }
    
        // Get id of entity to edit if not specified
        if (!$id) $id = \Input::get('id');
        
        // Perform the base query
        $query = $this->baseQuery();
        
        // Get the entity
        $entity = $query->where($this->config['table'] . '.' . $this->config['primaryKey'], $id)->first();
        
        // Process the form input and get data to insert
        $data = $this->processFormInput($this->config['edits'], $entity); 
        
        // Call after data is stored
        if (isset($this->config['callbacks']['beforeUpdate']) && is_callable($this->config['callbacks']['beforeUpdate'])) {
            $this->config['callbacks']['beforeUpdate']($data);
        }
        
        // Perform the update
        $query = $this->baseQuery();
        
        $query->where($this->config['table'] . '.' . $this->config['primaryKey'], $id)->update($data);
        
        // Call after data is stored
        if (isset($this->config['callbacks']['afterUpdate']) && is_callable($this->config['callbacks']['afterUpdate'])) {
            $this->config['callbacks']['afterUpdate']();
        }
        
        $this->redirect('Entry updated successfully');
    }
    
    private function performDestroy($id = NULL)
    {
        // Ensure this is allowed
        if (!$this->isAllowed('DELETE')) {
            abort(403, 'Deleting entries is not allowed.');
        }
    
        // Get id of entity to edit if not specified
        if (!$id) $id = \Input::get('id');
        
        // Perform the delete
        $query = $this->baseQuery()->where($this->config['table'] . '.' . $this->config['primaryKey'], $id);
        
        if (isset($this->config['softDeletes']) && $this->config['softDeletes']) {
            $query->update(['deleted_at'=>date('Y-m-d H:i:s')]);
        } else {
            $query->delete();
        }
        
        $this->redirect('Entry deleted successfully');
    }
    
    private function describe($table)
    {
        // Get the primary key for the main table
        $primary = \DB::select("show index from " . $table .  " where Key_name = 'PRIMARY'");
        
        // Set the primary key
        $this->config['primaryKey'] = $primary[0]->Column_name;
    
        // Get column details for main table
        $descriptions = \DB::select('DESC ' . $table);
        
        // Find the columns and fill in details
        foreach ($descriptions as $description) {
                
            // Determine type of column (if it has not been manually set)
            if (!isset($this->config['rules'][$description->Field]['type'])) {
            
                $type = explode(' ',str_replace(array('(',')',','),' ',$description->Type));
                
                switch ($type[0]) {
                    case 'tinyint':
                        $this->config['rules'][$description->Field]['type'] = 'checkbox';
                        break;
                
                    case 'int':
                        $this->config['rules'][$description->Field]['type'] = 'integer';
                        break;
                        
                    case 'timestamp';
                        $this->config['rules'][$description->Field]['type'] = 'timestamp';
                        break;
                        
                    case 'datetime':
                        $this->config['rules'][$description->Field]['type'] = 'datetime';
                        break;
                        
                    case 'date':
                        $this->config['rules'][$description->Field]['type'] = 'date';
                        break;
                        
                    case 'time':
                        $this->config['rules'][$description->Field]['type'] = 'time';
                        break;
                        
                    case 'varchar':
                        $this->config['rules'][$description->Field]['type'] = 'string';
                        break;
                        
                    case 'double':
                        $this->config['rules'][$description->Field]['type'] = 'double';
                        break;
                        
                    case 'enum':
                        $this->config['rules'][$description->Field]['type'] = 'select';
                        for ($i = 1; $i < count($type)-1; $i++) {
                            $value = str_replace("'", '', $type[$i]);
                            $title = ucwords($value);
                            $this->config['rules'][$description->Field]['options'][$value] = $title;
                        }
                        break;
                        
                    case 'text':
                    case 'mediumtext':
                    case 'longtext':
                        $this->config['rules'][$description->Field]['type'] = 'textarea';
                        break;
                }
            }
            
            // Determine whether this column is required (if it has not been manually set)
            if (!isset($this->config['rules'][$description->Field]['required'])) {
                ($description->Null == 'NO') ? $this->config['rules'][$description->Field]['required'] = true 
                                              : $this->config['rules'][$description->Field]['required'] = false;
            }
            
            // Determine default value of column
            if (!isset($this->config['rules'][$description->Field]['default'])) {
                ($description->Default != 'NULL') ? $this->config['rules'][$description->Field]['default'] = $description->Default
                                                  : $this->config['rules'][$description->Field]['default'] = '';
            }
            
            // Determine if this is an auto-increment column
            if ($description->Extra == 'auto_increment') {
                unset($this->creates[$description->Field]);
                unset($this->edits[$description->Field]);
            }
        }
    }
    
    protected function baseQuery($columns = NULL)
    {
        $query = \DB::table($this->config['table']);
        
        // If joins are specified, perform them
        if ($this->action == 'list') {
            foreach ($this->config['joins'] as $join) {
                $query->join($join['foreignTable'], $join['localKey'], '=', $join['foreignKey']);
            }
        }
        
        // If "where" is set, perform it
        if (isset($this->config['where'])) {
            $query->whereRaw($this->config['where']);
        }
        
        // If soft-delete is set...
        if (isset($this->config['softDeletes']) && $this->config['softDeletes']) {
            $query->whereNull('deleted_at');
        }
        
        $query->select($this->config['table'] . '.*');
        
        return $query;
    }
    
    protected function setupRelations()
    {
        foreach ($this->config['relations'] as $column=>$relation) 
        {
            $this->config['rules'][$column]['type'] = 'select';
            
            $query = \DB::table($relation['foreignTable']);
            
            if (isset($relation['where'])) {
                $query->whereRaw($relation['where']);
            }
            
            if (!$this->config['rules'][$column]['required']) {
                $this->config['rules'][$column]['options'][''] = NULL;
            }
            
            foreach ($query->get() as $foreignObject) {
                if (is_callable($relation['foreignTitle'])) {
                    $this->config['rules'][$column]['options'][$foreignObject->{$relation['foreignKey']}] = $relation['foreignTitle']($foreignObject);
                } else {
                    $this->config['rules'][$column]['options'][$foreignObject->{$relation['foreignKey']}] = $this->getForeignTitle($relation['foreignTitle'], $foreignObject);
                }
            }
            
            if (isset($relation['order']) && $relation['order']) {
                asort($this->config['rules'][$column]['options']);
            }
        }
    }
    
    protected function processFormInput($fields, $entity = NULL)
    {
        // Create array of data to update in database
        $data = array();
        
        // Make sure we only update the fields we want
        foreach ($fields as $name=>$select) {
            // TODO Validation
            
            // If this is an image upload
            if ($this->config['rules'][$name]['type'] == 'image_upload') {
            
                // If a file was given
                if (\Input::file($name)) {
                    // Set storage parameters
                    $uid = uniqid();
                    $path = $this->config['uploadsDir'] . $uid;
                    $file = \Input::file($name)->getClientOriginalName();
                
                    // Actually store the file
                    \Input::file($name)->move($path, $file);
                    
                    // Set the path to the file in the database
                    $data[$name] = $uid . '/' . $file;
                }
                
                // Otherwise use previous value
                else if ($entity) {
                    $data[$name] = $entity->{$name};
                } else {
                    $data[$name] = '';
                }
            }
            
            // If this is a file upload
            else if ($this->config['rules'][$name]['type'] == 'file_upload') {
            
                // If a file was given
                if (\Input::file($name)) {
                    // Set storage parameters
                    $uid = uniqid();
                    $path = $this->config['uploadsDir'] . $uid;
                    $file = \Input::file($name)->getClientOriginalName();
                
                    // Actually store the file
                    \Input::file($name)->move($path, $file);
                    
                    // Set the path to the file in the database
                    $data[$name] = $uid . '/' . $file;
                }
                
                // Otherwise use previous value
                else {
                    $data[$name] = $entity->{$name};
                }
            }
            
            // Otherwise treat it as a normal input
            else {
                $data[$name] = \Input::get($name);
                
                // TODO This is messy - needed to allow NULL in foreign relation selects
                if (!$data[$name]) {
                    $data[$name] = NULL;
                }
            }
        }
        
        // Set the values manually specified
        foreach ($this->config['values'] as $column=>$value) {
            $data[$column] = $value;
        }
        
        return $data;
    }
    
    protected function redirect($message)
    {
        // Compile params to parse to next action
        $params = ['_jcid'=>$this->config['jcid'],
                   '_msg'=>$message,
                   '_action'=>\Input::get('after'),
                   'id'=>\Input::get('id')];
                   
        exit(header('Location: ' . \URL::current().'?'.http_build_query($params)));
    }
    
    protected function getForeignTitle($title, $object)
    {
        $output = preg_replace_callback("/{{\s*([0-9a-zA-Z-_]*)\s*}}/", function ($match) use ($object) {
                // If the array key exists return it
                if (isset($object->{$match[1]})) {
                    return $object->{$match[1]};
                } else { // Otherwise just print the name of the variable
                    return $match[1];
                }
        }, $title);
        
        return $output;
    }
    
    protected function isAllowed($actions)
    {
        if (!is_array($actions)) {
            $actions = array($actions);
        }
        
        foreach ($actions as $action) {
            if (!in_array($action, $this->config['allowed'])) {
                return false;
            }
        }
        
        return true;
    }
}
