<?php namespace Jimtendo\JazzyCRUD;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->handleConfigs();
        $this->handlePublic();
        // $this->handleMigrations();
        $this->handleViews();
        // $this->handleTranslations();
        // $this->handleRoutes();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {

        return [];
    }

    private function handleConfigs() {

        $this->publishes([__DIR__ . '/../config/jazzycrud.php' => config_path('jazzycrud.php')], 'config');

        $this->mergeConfigFrom(__DIR__ . '/../config/jazzycrud.php', 'jazzycrud');
    }

    private function handleTranslations() {

        $this->loadTranslationsFrom('jazzycrud', __DIR__.'/../lang');
    }
    
    private function handlePublic()
    {
        $this->publishes([__DIR__.'/../public' => public_path('vendor/jimtendo/jazzycrud')], 'public');
    }

    private function handleViews()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'jazzycrud');

        $this->publishes([__DIR__.'/../views' => base_path('resources/views/vendor/jimtendo/jazzycrud')]);
    }

    private function handleRoutes() {

        include __DIR__.'/../routes.php';
    }
}
