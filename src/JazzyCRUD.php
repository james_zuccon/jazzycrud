<?php namespace Jimtendo\JazzyCRUD;

/**
* A terribly useful class
*
* See: http://knpuniversity.com/screencast/question-answer-day
*/
class JazzyCRUD
{
    public static function renderContainer($name)
    {
        return '<div id="' . $name . '" data-url="' . \URL::current('?container=' . $name) . '"></div>' .
               '<script>' .
               '     $("' . $name . '").jazzyBox({ headers:{ container:"' . $name . '"}" });' .
               '</script>';
    }
    
    public static function forContainer($name)
    {
    
    }
}
