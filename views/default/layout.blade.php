<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.css">

<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#table').dataTable();
    } );
</script>

@if (\Session::has('jcMessage'))
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  {{ \Session::get('jcMessage') }}
</div>
@endif

<?php $jcId = 'jccc'; ?>

<div id="{{ $jcId }}">
    <div class="jc-content"></div>
    <div class="jc-overlay"></div>
</div>

<script>
$(document).ready(function(){

    $('#{{ $jcId }}').on("submit click", "form, a", function(e) {
    
            // Prevent default event
            e.preventDefault();
            
            // Show ajax loading gif in overlay
            $('#{{ $jcId }} .jc-overlay').html('<center><img src="/img/ajax-loader.gif" /></center>').show();
        
            // If the event was a click event
            if (e.type == "click") {
                $.ajax(
                {
                    url : $(this).attr("href"),
                    type: "GET",
                    success:function(data, textStatus, jqXHR) 
                    {
                        $('#{{ $jcId }} .jc-content').html(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) 
                    {
                    }
                });
            }
            
            // Otherwise it was a submit event
            else {
                $.ajax(
                {
                    url : $(this).attr("action"),
                    type: "POST",
                    data : $(this).serializeArray(),
                    success:function(data, textStatus, jqXHR) 
                    {
                        $('#{{ $jcId }} .jc-content').html(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) 
                    {
                    }
                });
            }
            
            // Hide ajax loading gif overlay
            $('#{{ $jcId }} .jc-overlay').hide();
    });

}); 
</script>