<?php

/**
 * Your package config would go here
 */

return [
        
        /*
        |--------------------------------------------------------------------------
        | Theme to use
        |--------------------------------------------------------------------------
        |
        | The default theme to use
        |
        */

        'theme' => 'default',
        
        /*
        |--------------------------------------------------------------------------
        | Theme options
        |--------------------------------------------------------------------------
        |
        | The parameters here will vary depending on the theme. For the default
        | theme, we have the following options:
        | - loadBootstrap (bool) -> Should we load Bootstrap libraries
        | - loadJQuery (bool) -> Should we load JQuery libraries
        | - loadDataTables (bool) -> Should we load DataTables
        |
        | Generally, these should be set to false and included in your main layout
        | as including multiple times can cause problems.
        |
        */
        
        'themeOptions' => ['loadBootstrap'=>false, 'loadJQuery'=>false, 'loadDataTables'=>false],
        
        /*
        |--------------------------------------------------------------------------
        | Allowed Actions
        |--------------------------------------------------------------------------
        |
        | The actions that are allowed by default. Valid values are LIST,
        | CREATE, READ, UPDATE and DELETE
        |
        */

        'allowed' => ['LIST', 'CREATE', 'READ', 'UPDATE', 'DELETE'],
        
        /*
        |--------------------------------------------------------------------------
        | Upload directory
        |--------------------------------------------------------------------------
        |
        | The directory that files will be uploaded to.
        | (Root directory is public_path())
        |
        */
        'uploadsDir' => public_path() . '/uploads/',
        'uploadsUrl' => asset('/uploads') . '/',

];
