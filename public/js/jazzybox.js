(function ( $ ) {

    $.fn.jazzyBox = function(options) {
    
        var settings = $.extend({
            container: this,
            ajaxLoader: "/img/ajax-loader.gif",
        }, options );
      
        $.ajax(
        {
            url : this.data('url'),
            type: "GET",
            success:function(data, textStatus, jqXHR) 
            {
                $(settings.container).html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                $(settings.container).html('<center><img src="' + settings.ajaxLoader + '" /><br/><br/>' + errorThrown + '</center>');
            }
        });
      
        this.on("submit", "form", function(e) {
            
            if ($(this).data('prompt') && !confirm($(this).data('prompt'))) {
                return;
            }
          
            if ($(document.activeElement).attr('formtarget')) {
                return true;
            }
          
            e.preventDefault();
            


            var formURL = $(this).attr("action");
            var method = $(this).attr("method");
            
            if ($(document.activeElement).attr('formaction')) {
                formURL = $(document.activeElement).attr('formaction');
            }
            
            $(settings.container).html('<center><img src="' + settings.ajaxLoader + '" /></center>');
            
            $.ajax(
            {
                url : formURL,
                type: method,
                data: new FormData( this ),
                processData: false,
                contentType: false,
                success:function(data, textStatus, jqXHR) 
                {
                    $(settings.container).html(data);
                },
                error: function(jqXHR, textStatus, errorThrown) 
                {
                    alert(textStatus);
                    $(settings.container).html('<center><img src="' + settings.ajaxLoader + '" /><br/><br/>' + errorThrown + '</center>');
                }
            });
        });

        this.on("click", "a.jazzylink", function(e) {
        
            e.preventDefault();
        
            if ($(this).data('prompt') && !confirm($(this).data('prompt'))) {
                return;
            }
            
            var linkUrl = $(this).attr("href");
            
            $(settings.container).html('<center><img src="' + settings.ajaxLoader + '" /></center>');
            
            $.ajax(
            {
                url : linkUrl,
                type: "GET",
                success:function(data, textStatus, jqXHR) 
                {
                    $(settings.container).html(data);
                },
                error: function(jqXHR, textStatus, errorThrown) 
                {
                    $(settings.container).html('<center><img src="' + settings.ajaxLoader + '" /><br/><br/>' + errorThrown + '</center>');
                }
            });
        });
        
        return this;
    };

}( jQuery ));