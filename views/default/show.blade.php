@include('jazzycrud::' . $config['theme'] . '.functions')

@if (isset($config['renderHooks']['show']['before']))
  {!! $config['renderHooks']['show']['before'] !!}
@endif

{{-- Start Nav Tabs --}}
@if (count($config['tabs']))
<ul class="nav nav-tabs" role="tablist">
  <?php $tabNumber = 0; ?>
  @foreach ($config['tabs'] as $name=>$columns)
  <li role="presentation" class="{{ ($tabNumber == 0) ? 'active' : '' }}"><a href="#{{ str_slug($config['jcid'] . '-' . $name) }}" aria-controls="{{ str_slug($config['jcid'] . '-' . $name) }}" role="tab" data-toggle="tab">{{ $name }}</a></li>
  <?php $tabNumber++; ?>
  @endforeach
</ul>
@endif
{{-- End Nav Tabs --}}

@if (count($config['tabs']))
<div class="tab-content">

    <?php $tabNumber = 0; ?>
    @foreach ($config['tabs'] as $name=>$columns)
    <div role="tabpanel" class="tab-pane {{ ($tabNumber == 0) ? 'active' : '' }}" id="{{ str_slug($config['jcid'] . '-' . $name) }}">
    
        <table class="table table-striped table-bordered">
            <tbody>
                @foreach ($columns as $name)
                <tr>
                    <th>
                        {{ (isset($config['titles'][$name])) ? $config['titles'][$name] : cleanTitle($name) }}
                    </th>
                    <td>
                        @if (isset($config['rules'][$name]['escape']) && !$config['rules'][$name]['escape'])
                        {!! $entity->$name !!}
                        @else
                        {{ $entity->$name }}
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        
    </div>
    <?php $tabNumber++; ?>
    @endforeach
</div>

@else

<table class="table table-striped table-bordered">
    <tbody>
        @foreach ($shows as $name=>$select)
        <tr>
            <th>
                {{ (isset($config['titles'][$name])) ? $config['titles'][$name] : cleanTitle($name) }}
            </th>
            <td>
                @if (isset($config['rules'][$name]['escape']) && !$config['rules'][$name]['escape'])
                {!! $entity->$name !!}
                @else
                {{ $entity->$name }}
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endif

@if (isset($config['renderHooks']['show']['after']))
  {!! $config['renderHooks']['show']['after'] !!}
@endif

<div class="text-right">
    @foreach ($entity->actions as $name=>$action)
	{!! renderAction($config['jcid'], $name, $action) !!}
    @endforeach
</div>

