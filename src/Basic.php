<?php namespace Jimtendo\JazzyCRUD;

/**
* A terribly useful class
*
* See: http://knpuniversity.com/screencast/question-answer-day
*/
class Basic extends Base
{
    function __construct($jcid, $config = array())
    {
        parent::__construct($jcid, $config);
    }

    public function lists($columns)
    {
        $this->config['lists'] = $this->toAssoc($columns);
        
        // If edits isn't set, then set it the same
        if (!$this->config['edits']) {
            $this->edits($columns);
        }
        
        return $this;
    }
    
    public function edits($columns)
    {
        $this->config['edits'] = $this->toAssoc($columns);
        $this->config['creates'] = $this->toAssoc($columns);
        $this->config['shows'] = $this->toAssoc($columns);
        
        return $this;
    }
    
    public function addAction($on, $action, $title = NULL, $icon = NULL)
    {
        $this->actions[$on][$action] = function($row) use ($title, $icon) { return ['type'=>'custom', 'title'=>$title, 'classes'=>'glyphicon ' . $icon]; };
        
        return $this;
    }

    private function toAssoc(array $array)
    {
        foreach ($array as $key=>$value) {
            if ($key !== $value && is_numeric($key)) {
                unset($array[$key]);
                $array[$value] = $value;
            }
        }
        
        return $array;
    }
}
